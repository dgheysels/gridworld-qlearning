/*
 * 
 * INFO : Multiagent Reinforcement Learning for Multi-Robot Systems: A Survey
 */

package algorithm;

import general.*;
import java.util.*;

public class Qlearning {
	
	public static final float ALPHA = 0.2f;   // learning factor
	public static final float GAMMA = 0.9f;   // discount value
	public static final float s     = 0.001f; // bepaald de daling van temperatuur 
	public static final float NOISE = 0.0f;  // rate of noise
	
	private static		 float initialTemperature = 1.0f;
	private static       float temperature = initialTemperature;
	private static       int   episodeCounter = 0;
	
	private Agent _parent;
	private HashMap<State, EnumMap<Action, Float>> _qvalues;
	
	public Qlearning(Agent robot) {
		_parent = robot;
		_qvalues = new HashMap<State, EnumMap<Action, Float>>();	
	}

	public Action chooseAction(State currentState) {
				
		Action choice = null;
		Float max = 0.0f;
		float random = (float)Math.random();
		float noise  = (float)Math.random();
		
		// exploreren
		switch((int)(Math.random() * 4)) {
			case 0 : choice = Action.up; break;
			case 1 : choice = Action.left; break;
			case 2 : choice = Action.right; break;
			case 3 : choice = Action.down; break;
		}
		
		// exploitateren als de temperatuur daalt
		if (random > temperature) {
						
			if (_qvalues.get(currentState) == null) {
				_qvalues.put(currentState, new EnumMap<Action, Float>(Action.class));
				EnumMap<Action, Float> actions = _qvalues.get(currentState);
				actions.put(Action.up, new Float(0));
				actions.put(Action.left, new Float(0));
				actions.put(Action.right, new Float(0));
				actions.put(Action.down, new Float(0));
			}
			
			EnumMap<Action, Float> actions = _qvalues.get(currentState);
			
			// kies beste actie adhv qValue
			for (Map.Entry<Action, Float> e : actions.entrySet()) {
				if (e.getValue().compareTo(max) > 0) {
					max = e.getValue();
					choice = e.getKey();
				}
			}
		} 
		
		if (noise < NOISE) { // noise aanwezig			
			if (choice == Action.up)
				choice = Action.down;
			else if (choice == Action.down)
				choice = Action.up;
			else if (choice == Action.left)
				choice = Action.right;
			else if (choice == Action.right)
				choice = Action.left;
		}
		
		return choice;
	}
	
	// state = previousState van agent
	public void updateQValue(State state, Action a, float reward) {
		
		if (_qvalues.get(state) == null) {
			_qvalues.put(state, new EnumMap<Action, Float>(Action.class));
			EnumMap<Action, Float> actions = _qvalues.get(state);
			actions.put(Action.up, new Float(0));
			actions.put(Action.left, new Float(0));
			actions.put(Action.right, new Float(0));
			actions.put(Action.down, new Float(0));
		}
		
		EnumMap<Action, Float> actions = _qvalues.get(state);
		
		float oldQ = getQValue(state, a);		
		float newQ = 
			(1-ALPHA)*oldQ + 
			ALPHA * (reward + (GAMMA * getMaxQValue(_parent.getCurrentState())));
		
		actions.put(a, new Float(newQ));
		_qvalues.put(state, actions);
	}
	
	public void reset() {
		for (EnumMap action : _qvalues.values()) {
			action.clear();
		}
		_qvalues.clear();
		
		temperature = initialTemperature;
		episodeCounter = 0;
	}
	
	private float getMaxQValue(State state) {
		
		if (_qvalues.get(state) == null) {
			_qvalues.put(state, new EnumMap<Action, Float>(Action.class));
			EnumMap<Action, Float> actions = _qvalues.get(state);
			actions.put(Action.up, new Float(0));
			actions.put(Action.left, new Float(0));
			actions.put(Action.right, new Float(0));
			actions.put(Action.down, new Float(0));
		}
		
		EnumMap<Action, Float> actions = _qvalues.get(state);
		Float bestQ = Collections.max(actions.values());
		
		return (bestQ == null) ? 0 : bestQ.floatValue();
	}
	
	private float getQValue(State state, Action action) {
		return _qvalues.get(state).get(action).floatValue();
	}
	
	public static void newEpisode() {
		episodeCounter++;
		temperature = (float)Math.exp(-s * episodeCounter) * (initialTemperature+1);	
	}
	
	public static float getTemperature() {
		return temperature;
	}
	
	public HashMap<State, EnumMap<Action, Float>> getQValues() { return _qvalues; }
}
