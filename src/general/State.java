package general;
public class State  {
	
	private Agent _agent;
	private boolean _isGoalState;
	private Agent   _goalAgent;
	
	public State(boolean goalState) {
		_agent = null;
		_isGoalState = goalState;
	}
	
	public Agent getAgent() { return _agent; }
	public void setAgent(Agent r) { _agent = r; }
	public boolean isGoalState() { return _isGoalState; }
	public void setGoalState(boolean goal, Agent agent) { _isGoalState = goal; _goalAgent = agent; }
	public Agent getGoalAgent() { return _goalAgent; }
	
	public boolean isEmpty() {
		return (_agent == null) ? true : false;
	}
}
