package general;
import java.util.EnumMap;
import java.util.HashMap;

import gui.GridModel;

import algorithm.Qlearning;

public class Agent {
	
	private GridModel _environment;

	private State _startState;
	private State _goalState;
	private State _currentState;
	private Qlearning _qlearning;
	private int _steps;
	private String _name;

	public Agent(String name) {
		_steps = 0;
		_name = name;
		_qlearning = new Qlearning(this);
	}


	public GridModel getEnvironment() { return _environment; }
	public void setEnvironment(GridModel environment) { _environment = environment; }

	public boolean atDestination() {
		return (_currentState.equals(_goalState));
	}
	
	public void performAction() {	
		if (!atDestination()) {
			State previousState = _currentState;
			Action action = chooseAction();
			float reward = _environment.moveAgent(this, action);
			_steps++;
			_qlearning.updateQValue(previousState, action, reward);
		}
	}
	
	private Action chooseAction() {	
		return _qlearning.chooseAction(_currentState);	
	}
	
	public HashMap<State, EnumMap<Action, Float>> getQValues() {
		return _qlearning.getQValues();
	}
	
	public void reset() {
		_qlearning.reset();
		setCurrentState(_startState);
		clearSteps();
	}
	
	public int getSteps() { return _steps; }
	public void clearSteps() { _steps = 0; }
	public String getName() { return _name; }

	public State getStartState() { return _startState; }
	public void setStartState(State s) { 
		_startState = s; 
		_startState.setAgent(this);
		_currentState = _startState;
	} 
	
	public State getCurrentState() { return _currentState; }
	public void setCurrentState(State s) { 	
		if (_currentState.getAgent().equals(this)) {
			_currentState.setAgent(null);
		}
	
		_currentState = s;
		_currentState.setAgent(this);	
	}
	
	public State getGoalState() { return _goalState; }
	public void setGoalState(State s) { _goalState = s; _goalState.setGoalState(true, this); }
}
