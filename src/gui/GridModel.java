package gui;

import general.*;

import java.util.*;
import javax.swing.text.*;
import javax.swing.event.*;


import sun.util.calendar.CalendarDate;
import algorithm.Qlearning;

public class GridModel extends PlainDocument {

	private EventListenerList listeners = new EventListenerList();

	private int _width;
	private int _height;
	
	private Vector<State> _states;
	private Vector<Agent> _agents;
	
	private HashMap<Agent, State> _backupStartPositions;
	
	public GridModel() {}
	public GridModel(int w, int h) {
		_width = w;
		_height = h;
		_agents = new Vector<Agent>(0);
		_backupStartPositions = new HashMap<Agent, State>();
		_states = new Vector<State>();
		_states.setSize(_width * _height);
		for (int i=0; i<_states.size(); i++) {
			_states.set(i, new State(false));
		}
	}
	
	public void addAgent(Agent agent, int startX, int startY, int goalX, int goalY) {
		agent.setEnvironment(this);
		
		agent.setStartState(_states.get(startX + startY * _height));
		agent.setGoalState(_states.get(goalX + goalY * _height));
		_agents.add(agent);	
	}

	// verplaatst de robot en geeft een reward
	// buiten de grenzen = -1000
	// op bezette plaats = -100
	// op goalstate      = +100
	// andere gevallen   = 0
	public float moveAgent(Agent agent, Action action) {
				
		State currentState = agent.getCurrentState();
		int currentPosition = _states.indexOf(currentState);		
		int newPosition = -1;
		float reward = 0;
		
		//controle of de move wel binnen de grenzen is
		// indien erbuiten wordt de move afgebroken voor deze agent
		if ((action == Action.up) && 
			(currentPosition >= _width)) {
			newPosition = currentPosition - _width;				
		}
		else if ((action == Action.left) && 
				 (currentPosition % _width) != 0) {
			newPosition = currentPosition - 1;
		}
		else if ((action == Action.right) && 
				(currentPosition % _width) != _width - 1) {
			newPosition = currentPosition + 1;
		}
		else if ((action == Action.down) &&
				(currentPosition < _width * (_height - 1))) {
			newPosition = currentPosition + _width;
		}

		// indien de move binnen de boundaries was
		if (newPosition != -1) {	
			if (!free(newPosition)) {
				reward = -50.0f;
			}
			else {
				agent.setCurrentState(_states.get(newPosition));
				_states.get(currentPosition).setAgent(null);
				_states.get(newPosition).setAgent(agent);
				
				if (agent.atDestination()) {
					reward = 100.0f;
				}
				else {
					reward = 0.0f;
				}
			}
		}
		else {
			reward = -500.0f;
		}
				
		return reward;		
	}
	
	public boolean allAgentsAtDestination() {
		for (Agent r : _agents) {
			if (!r.atDestination()) {
				return false;
			}
		}
		return true;
	}
	
	public void training() {	
		System.out.println("training...");
		backupAgentPositions();
		
		long t0 = Calendar.getInstance().getTimeInMillis();
		
		for (Agent a : _agents) {
			try {
				insertString(getEndPosition().getOffset(), ";" + a.getName(), new SimpleAttributeSet());	
				
			}
			catch(BadLocationException ex) {
				ex.printStackTrace();
			}
		}
		
		for (int i=0; i<2000; i++) {
			Qlearning.newEpisode();
						
			while (!allAgentsAtDestination()) {
				for (Agent a : _agents) {
					a.performAction();
				}
			}
			
			try {
				insertString(getEndPosition().getOffset(), "\n" + i + ";", new SimpleAttributeSet());
				System.out.println(i + ":" + Qlearning.getTemperature());
				
				for (Agent a : _agents) {
					insertString(getEndPosition().getOffset(), a.getSteps() + ";", new SimpleAttributeSet());
					a.clearSteps();
				}	
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			
			restoreAgentPositions();	
		}	
		
		try {
			insertString(getEndPosition().getOffset(), "\ntraining time: " + (Calendar.getInstance().getTimeInMillis() - t0) + "ms\n", new SimpleAttributeSet());
		}
		catch(BadLocationException ex) {
			ex.printStackTrace();
		}
	}
	
	public void step() {
		for (Agent agent : getAgents()) {
			agent.performAction();
			fireStateChanged();
		}
	}
	
	public void run() {
		
		long t0 = Calendar.getInstance().getTimeInMillis();
		
		while (!allAgentsAtDestination()) {
			step();
			try {
				Thread.sleep(250);
			}
			catch(InterruptedException ex) {
				ex.printStackTrace();
			}
		}
		
		
		
		try {
			
			for (Agent a : _agents) {
				insertString(getEndPosition().getOffset(), a.getSteps() + ";", new SimpleAttributeSet());
				a.clearSteps();
			}
			
			insertString(getEndPosition().getOffset(), "\nrunning time: " + (Calendar.getInstance().getTimeInMillis() - t0) + "ms", new SimpleAttributeSet());
		}
		catch(BadLocationException ex) {
			ex.printStackTrace();
		}
	}
	
	public void reset() {
		for (Agent a : _agents) {
			a.reset();
		}
						
		try {
			insertString(getEndPosition().getOffset(), "\n *** RESET *** \n", new SimpleAttributeSet());
		}
		catch(BadLocationException ex) {
			ex.printStackTrace();
		}

		fireStateChanged();
		
	}
	
	// move naar een lege state
	private boolean free(int position) {
		return _states.get(position).isEmpty();
	}
	
	private void backupAgentPositions() {
		for (Agent a : _agents) {
			_backupStartPositions.put(a, a.getCurrentState());
		}
	}
	
	private void restoreAgentPositions() {
		for (Agent a : _agents) {
			a.setCurrentState(_backupStartPositions.get(a));
			a.clearSteps();
		}
	}
	
	public void fireStateChanged() {
		ChangeListener[] list = listeners.getListeners(ChangeListener.class);
		for (int i=0; i<list.length; i++) {
			list[i].stateChanged(new ChangeEvent(this));
		}
	}

	public void addChangeListener(ChangeListener listener) {
		listeners.add(ChangeListener.class, listener);
	}

	public Vector<State> getStates() { return _states; }
	public Vector<Agent> getAgents() { return _agents; }
	public void setAgents(Vector<Agent> agents) { _agents = agents; }
	public int getHeight() { return _height; }
	public int getWidth() { return _width; }
}
