package gui;
import general.Agent;
import general.State;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;

public class GridUI extends ComponentUI {

	public static final int CELL_WIDTH = 50;
	public static final int CELL_HEIGHT = 50;
	
	public void paint(Graphics g, JComponent c) {
		
		Graphics2D g2d = (Graphics2D)g;
		GridModel model = ((GridComponent)c).getModel();
		Vector<State> states = model.getStates();
		java.awt.Point loc = c.getLocation();
		int x = (int) loc.getX();
		int y = (int) loc.getY();
		c.setBounds(x, y, CELL_WIDTH * model.getWidth() + 1, CELL_HEIGHT * model.getHeight() + 1);
		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, CELL_WIDTH * model.getWidth() + 1, CELL_HEIGHT * model.getHeight() + 1);
		g2d.setColor(Color.BLACK);
		
		for (int i = 0; i < model.getHeight(); i++) {
			for (int j = 0; j < model.getWidth(); j++) {
				g2d.drawRect(i * CELL_WIDTH, j * CELL_HEIGHT, CELL_WIDTH,
						CELL_HEIGHT);
				
				State s = states.get(j + i * model.getWidth());
				Agent r = s.getAgent();
								
				if (r != null) {
					g2d.drawString(r.getName(), 
							j*CELL_WIDTH + 2, i*CELL_HEIGHT + 15);
				}		
				
				if (s.isGoalState()) {
					g2d.setColor(Color.RED);
					g2d.drawString("DEST " + s.getGoalAgent().getName(), 
							j*CELL_WIDTH + 2, i*CELL_HEIGHT + CELL_HEIGHT - 5);
					g2d.setColor(Color.BLACK);
					
					
				}
				
			}
		}
				
	}
	
	
	public static ComponentUI createUI(JComponent c) {
		return new GridUI();
	}
    
    public void installUI(JComponent c) {}
	public void uninstallUI(JComponent c) {}
	
}
