package gui;

import general.Action;
import general.Agent;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class GridComponent extends JComponent implements ChangeListener {

	
	private GridModel _model;
    
    	public GridComponent() {}  
	public GridComponent(GridModel model) {
		_model = model;	
		_model.addChangeListener(this);
	}
	
	public void addAgent(Agent agent, int startX, int startY, int goalX, int goalY) {
		_model.addAgent(agent, startX, startY, goalX, goalY);
	}
	
	public void moveAgent(Agent agent, Action action) {
		_model.moveAgent(agent, action);
	}

	public void train() {
		_model.training();
	}

	public void reset() {
		_model.reset();
	}

	public void step() {
		_model.step();
	}

	public void run() {
		_model.run();
	}
	
	public GridModel getModel() { return _model; }
	
	/*
	 * Methods voor ComponentUI-delegate
	 * Deze worden gebruikt door het Swing-Framework
	 */
	public void setUI(GridUI componentUI) {
		super.setUI(componentUI);
	}
	
	public void updateUI() {
		setUI((GridUI)UIManager.getUI(this));
		invalidate();
	}
	
	public String getUIClassID() {
		return "GridUI";
	}
	
	public void setSize() {
		setMinimumSize(new Dimension(getWidth(), getHeight()));
		setPreferredSize(new Dimension(getWidth(), getHeight()));
		setSize(new Dimension(getWidth(), getHeight()));
	
		updateUI();
	}

	public void stateChanged(ChangeEvent e) {
		repaint();
	}
}
