package gui;

import general.Agent;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.*;



public class Gridworld extends JFrame implements ActionListener {
	
	private GridModel gridModel;
	private GridComponent grid;

	public Gridworld() {
		initGUI();
	}

	private void initGUI() {
		Container content = getContentPane();
		content.setLayout(new BorderLayout()); 
	
		Agent r1 = new Agent("R1");
		Agent r2 = new Agent("R2");
		Agent r3 = new Agent("R3");
		Agent r4 = new Agent("R4");
		Agent r5 = new Agent("R5");
		Agent r6 = new Agent("R6");
		Agent r7 = new Agent("R7");
		Agent r8 = new Agent("R8");
		Agent r9 = new Agent("R9");
		Agent r10 = new Agent("R10");
		Agent r11 = new Agent("R11");
		Agent r12 = new Agent("R12");
		Agent r13 = new Agent("R13");
		
		gridModel = new GridModel(4, 4);
		grid = new GridComponent(gridModel);
				                              
		grid.addAgent(r1, 0, 0, 3, 3);
		grid.addAgent(r2, 3, 3, 0, 0);
		grid.addAgent(r3, 0, 3, 3, 0);
		grid.addAgent(r4, 3, 0, 0, 3);
		//grid.addAgent(r5, 0, 2, 3, 1);
		//grid.addAgent(r6, 0, 1, 3, 2);
		//grid.addAgent(r7, 2, 0, 1, 3);
		
/*
		gridModel = new GridModel(10, 10);
		grid = new GridComponent(gridModel);
				                              
		grid.addAgent(r1, 0, 2, 8, 7);
		grid.addAgent(r2, 4, 8, 3, 2);
		grid.addAgent(r3, 9, 1, 4, 6);
		grid.addAgent(r4, 4, 6, 9, 4);
		grid.addAgent(r5, 0, 8, 0, 0);
		grid.addAgent(r6, 0, 9, 6, 2);
		grid.addAgent(r7, 9, 3, 4, 9);
*/		
		
		
/*
		gridModel = new GridModel(17, 17);
		grid = new GridComponent(gridModel);

		grid.addAgent(r1, 0, 7, 12, 9);
		grid.addAgent(r2, 9, 13, 8, 16);
		grid.addAgent(r3, 16, 16, 3, 5);
		grid.addAgent(r4, 3, 2, 0, 6);
		grid.addAgent(r5, 12, 9, 3, 12);
		grid.addAgent(r6, 15, 5, 5, 15);
		grid.addAgent(r7, 5, 10, 9, 12);
		grid.addAgent(r8, 8, 12, 2, 0);
		grid.addAgent(r9, 1, 13, 16, 3);
		grid.addAgent(r10, 7, 3, 17, 7);
		grid.addAgent(r11, 13, 3, 12, 13);
		grid.addAgent(r12, 3, 10, 8, 14);
		grid.addAgent(r13, 6, 8, 3, 16);
*/

		
		grid.setSize();
			
		content.add(grid, BorderLayout.CENTER);

		// trainen
		JButton trainButton = new JButton("train");
		trainButton.addActionListener(this);
		trainButton.setActionCommand("training");
		

		// outputarea
		JTextArea output = new JTextArea(gridModel);
		output.setRows(10);
		JScrollPane outputScroller = new JScrollPane(output);
			
		
		// stap voor stap
		JButton stepButton = new JButton("Step");
		stepButton.addActionListener(this);
		stepButton.setActionCommand("step");
		
		// in een keer
		JButton endButton = new JButton("Run");
		endButton.addActionListener(this);
		endButton.setActionCommand("run");
	
		// reset
		JButton resetButton = new JButton("reset");
		resetButton.addActionListener(this);
		resetButton.setActionCommand("reset");
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		buttonPanel.add(trainButton);
		buttonPanel.add(stepButton);
		buttonPanel.add(endButton);
		buttonPanel.add(resetButton);
		
		content.add(buttonPanel, BorderLayout.WEST);
		content.add(outputScroller, BorderLayout.SOUTH);
		
		setSize(600, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		pack();
	}



	public static void main(String[] args) throws Exception {
		
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		UIManager.put("GridUI", "gui.GridUI");
		
		Gridworld gw = new Gridworld();		
		gw.setVisible(true);		
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "training") {
			
			Runnable r = new Runnable() {
				public void run() {
					gridModel.training();
				}
			};
		
			new Thread(r).start();
		}
		if (e.getActionCommand() == "step") {
			gridModel.step();
			
		} 
		if (e.getActionCommand() == "run") {
			
			Runnable r = new Runnable() {
				public void run() {
					gridModel.run();
				}
			};

			new Thread(r).start();
			
		}
		if (e.getActionCommand() == "reset") {
			gridModel.reset();
			
		}
	}
}
